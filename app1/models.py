# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class MyModel(models.Model):

    foo = models.CharField(max_length=100)
    bar = models.URLField(max_length=255, blank=True)
